FROM node:carbon
WORKDIR /usr/src/app 

RUN apt-get install git-core 
RUN git clone https://iepctsergio@bitbucket.org/iepctsergio/api_informacion.git
WORKDIR /usr/src/app/api_informacion
RUN mkdir uploads
RUN npm install
CMD [ "npm", "start" ]