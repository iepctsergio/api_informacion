'use strict'

var Global = require('./global');

var app = require('./app');
let redis = require('redis');

const canal = 'nuevapublicacion';

let redisClient = redis.createClient(Global.Global.urlredis);
var port = process.env.PORT || 3005;
app.listen(port, function () {
    console.log('Servidor de api rest escuchando en el puerto: ' + port);
});

redisClient.subscribe(canal);

